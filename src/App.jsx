import { useState } from 'react'
import reactLogo from './assets/react.svg'
import './App.css'
import CUno from './Components/CUno'
import CDos from './Components/CDos'
import CTres from './Components/CTres'

function App() {

  return (
    <div>
      <CUno/>
      <CDos/>
      <CTres/>
    </div>
  )
}

export default App
